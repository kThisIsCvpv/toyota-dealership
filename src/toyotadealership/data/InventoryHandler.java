package toyotadealership.data;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Scanner;

import toyotadealership.car.BaseCar;

public class InventoryHandler {

	private List<BaseCar> currentInventory;
	private List<Invoice> pastSales;

	private double tax = 0.07;
	private double flatChargeAmount = 100;
	private SimpleDateFormat dateFormat;

	private Scanner sin;

	public InventoryHandler(List<BaseCar> currentInventory, List<Invoice> pastSales, double tax, double flatChargeAmount, SimpleDateFormat dateFormat) {
		this.currentInventory = currentInventory;
		this.pastSales = pastSales;
		this.tax = tax;
		this.flatChargeAmount = flatChargeAmount;
		this.dateFormat = dateFormat;
		this.beginInterface();
	}

	private void beginInterface() {
		sin = new Scanner(System.in);

		String in;
		do {
			System.out.println("You are using the Toyota Dealership Inventory Manager Program!");
			System.out.println("1 - Sell a Vehicle");
			System.out.println("2 - Display Unsold Vehicles");
			System.out.println("3 - Display All Vehicle Sales");
			System.out.println("4 - Exit the Program");

			int optionIndex = -1;
			while (optionIndex == -1) {
				System.out.print("\nPlease select an option from the menu above: ");
				in = this.sin.nextLine();

				try {
					optionIndex = Integer.parseInt(in);
				} catch (NumberFormatException ex) {
					System.out.println("You have selected an invalid method number! Please try again.");
					continue;
				}

				if (optionIndex < 1 || optionIndex > 4) {
					System.out.println("You have selected an invalid method number! Please try again.");
					optionIndex = -1;
				}
			}

			if (optionIndex == 1) {
				if (this.currentInventory.isEmpty()) {
					System.out.println("The Inventory is Empty! You cannot make a sale!");
					continue;
				}

				System.out.println("\nThe Following Lists the models in the Current Inventory:");
				for (int i = 0; i < this.currentInventory.size(); i++) {
					BaseCar car = this.currentInventory.get(i);
					System.out.printf("#" + (i + 1) + " > " + car.getModel() + " ( $%.2f - " + car.getVehicleIdentificationNumber() + " ) %s\n", car.getPrice(), car.getAdditionalInfo());
				}

				int toSell = -1;
				while (toSell == -1) {
					System.out.print("\nPlease enter the car you would like to sell: ");

					in = this.sin.nextLine();
					try {
						toSell = Integer.parseInt(in);
					} catch (NumberFormatException ex) {
						System.out.println("You have selected an invalid method number! Please try again.");
						continue;
					}

					if (toSell < 1 || toSell > this.currentInventory.size()) {
						System.out.println("You have selected an invalid method number! Please try again.");
						toSell = -1;
					}
				}

				BaseCar vehicle = this.currentInventory.get(toSell - 1);
				double tax = this.tax * vehicle.getPrice();
				double netPrice = vehicle.getPrice() + tax + this.flatChargeAmount;
				long salesDate = System.currentTimeMillis();
				System.out.println("\nThe Following Shows the Vehicle's Information and Pricing...\n");
				vehicle.printDetails();
				
				System.out.printf("\nTax Amount: $%.2f\n", tax);
				System.out.printf("Flat Charge: $%.2f\n", this.flatChargeAmount);
				System.out.printf("Total Price: $%.2f\n", netPrice);
				System.out.println("Sales Date: " + this.dateFormat.format(salesDate));
				
				System.out.print("\nPlease input the Customer's Name: ");
				String customerName = this.sin.nextLine();
				System.out.print("\nPlease input the Customer's Address: ");
				String customerAddress = this.sin.nextLine();
				System.out.print("\nPlease input the Customer's Phone Number: ");
				String customerPhoneNumber = this.sin.nextLine();

				Invoice sale = new Invoice(customerName, customerAddress, customerPhoneNumber, vehicle, salesDate, vehicle.getPrice());
				this.pastSales.add(sale);
				this.currentInventory.remove(vehicle);
				System.out.println("\nCongratulations! You have successfully sold the vehicle!");
			} else if (optionIndex == 2) {
				System.out.println("\nThe Following List Displays the Current Inventory (" + this.currentInventory.size() + " entries):");
				if (this.currentInventory.isEmpty())
					System.out.println("> The Inventory is Empty");
				else
					for (int i = 0; i < this.currentInventory.size(); i++) {
						if (i == 0)
							System.out.println();

						this.currentInventory.get(i).printDetails();

						if (i != this.currentInventory.size() - 1)
							System.out.println();
					}
			} else if (optionIndex == 3) {
				System.out.println("\nThe Following List Displays All Sales Data (" + this.pastSales.size() + " entries):");
				if (this.pastSales.isEmpty())
					System.out.println("> No Previous Sales Made");
				else
					for (int i = 0; i < this.pastSales.size(); i++) {
						if (i == 0)
							System.out.println();

						this.pastSales.get(i).printSalesDetails(this.dateFormat);

						if (i != this.pastSales.size() - 1)
							System.out.println();
					}

			} else if (optionIndex == 4) {
				this.printSpacer();
				System.out.println("Thank you for using the Toyota Dealership Inventory Manager Program!");
				return;
			}

			this.printSpacer();
		} while (true);

	}

	private void printSpacer() {
		System.out.println("\n--------------------------------------------------------------\n");
	}
}
