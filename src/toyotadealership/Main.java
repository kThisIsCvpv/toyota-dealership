package toyotadealership;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import toyotadealership.car.BaseCar;
import toyotadealership.car.Car;
import toyotadealership.car.SUV;
import toyotadealership.car.Truck;
import toyotadealership.data.InventoryHandler;
import toyotadealership.data.Invoice;

public class Main {

	public static final double TAX_PERCENTAGE = 0.07;
	public static final double FLAT_CHARGE = 100;
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	public static void main(String[] args) {
		List<BaseCar> currentInventory = new ArrayList<BaseCar>();
		
		currentInventory.add(new Car("1FUJA6CV66LV40478", "Lotus Boson", 1999, 50000, 4, false, false));
		currentInventory.add(new Car("1GKS1BKC2FR143730", "Saab Squarks", 2012, 75000, 2, true, false));
		currentInventory.add(new Car("YV4CZ592761234690", "Volkswagen Neutrino", 2002, 122000, 4, true, true));
		
		currentInventory.add(new SUV("1B4HS28Z4XF654953", "GM Branon", 2008, 42000, false, true, true));
		currentInventory.add(new SUV("4T1BF18B0XU298079", "Hyundai Neutron", 2002, 12000, false, false, true));
		
		currentInventory.add(new Truck("1G6KD54YX2U274198", "Mazda Higgs", 2001, 52000, false, true));
		currentInventory.add(new Truck("JTLKE50E581049174", "Buick Photino", 2007, 61000, false, true));
		
		new InventoryHandler(currentInventory, new ArrayList<Invoice>(), TAX_PERCENTAGE, FLAT_CHARGE, DATE_FORMAT);
	}

}
