package toyotadealership.car;

import java.util.HashMap;

public class SUV extends BaseCar {

	private boolean sunRoof;
	private boolean leatherInterior;
	private boolean roofRack;

	public SUV(String viNumber, String model, int year, double price, boolean sunRoof, boolean leatherInterior, boolean roofRack) {
		super(viNumber, model, year, price);
		this.sunRoof = sunRoof;
		this.leatherInterior = leatherInterior;
		this.roofRack = roofRack;
	}

	public boolean hasSunRoof() {
		return this.sunRoof;
	}

	public boolean hasLeatherInterior() {
		return this.leatherInterior;
	}

	public boolean hasRoofRack() {
		return this.roofRack;
	}

	@Override
	public void printDetails() {
		super.printDetails();
		System.out.println("Sun Roof: " + (this.sunRoof ? "Included" : "Excluded"));
		System.out.println("Leather Interior: " + (this.leatherInterior ? "Included" : "Excluded"));
		System.out.println("Roof Rack: " + (this.roofRack ? "Included" : "Excluded"));
	}

	@SuppressWarnings("serial")
	@Override
	public String getAdditionalInfo() {
		return new HashMap<String, String>() {
			{
				this.put("Sun Roof", (sunRoof ? "Included" : "Excluded"));
				this.put("Leather Interior", (leatherInterior ? "Included" : "Excluded"));
				this.put("Roof Rack", (roofRack ? "Included" : "Excluded"));
			}
		}.toString();
	}
}
