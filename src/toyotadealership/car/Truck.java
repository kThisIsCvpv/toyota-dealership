package toyotadealership.car;

import java.util.HashMap;

public class Truck extends BaseCar {

	private boolean bedLiner;
	private boolean trailerHitch;

	public Truck(String viNumber, String model, int year, double price, boolean bedLiner, boolean trailerHitch) {
		super(viNumber, model, year, price);
		this.bedLiner = bedLiner;
		this.trailerHitch = trailerHitch;
	}

	public boolean hasBedLiner() {
		return this.bedLiner;
	}

	public boolean hasTrailerHitch() {
		return this.trailerHitch;
	}
	
	@Override
	public void printDetails() {
		super.printDetails();
		System.out.println("Bed Liner: " + (this.bedLiner ? "Included" : "Excluded"));
		System.out.println("Trailer Hitch: " + (this.trailerHitch ? "Included" : "Excluded"));
	}

	@SuppressWarnings("serial")
	@Override
	public String getAdditionalInfo() {
		return new HashMap<String, String>() {
			{
				this.put("Bed Liner", (bedLiner ? "Included" : "Excluded"));
				this.put("Trailer Hitch", (trailerHitch ? "Included" : "Excluded"));
			}
		}.toString();
	}
}
