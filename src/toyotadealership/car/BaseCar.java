package toyotadealership.car;

public abstract class BaseCar {

	private String viNumber;
	private String model;
	private int year;
	private double price;

	protected BaseCar(String viNumber, String model, int year, double price) {
		this.viNumber = viNumber;
		this.model = model;
		this.year = year;
		this.price = price;
	}

	public String getVehicleIdentificationNumber() {
		return this.viNumber;
	}

	public String getModel() {
		return this.model;
	}

	public int getYear() {
		return this.year;
	}

	public double getPrice() {
		return this.price;
	}

	public void printDetails() {
		System.out.println("Vehicle Identification Number: " + this.viNumber);
		System.out.println("Model Identification: " + this.model + " (" + (super.getClass().getSimpleName()) + ")");
		System.out.println("Manufactured Year: " + this.year);
		System.out.printf("Base Price: $%.2f\n", this.price);
	}

	public String getAdditionalInfo() {
		return "";
	}
}
