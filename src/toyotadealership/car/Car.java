package toyotadealership.car;

import java.util.HashMap;

public class Car extends BaseCar {

	private int bodyStyle;
	private boolean sunRoof;
	private boolean leatherInterior;

	public Car(String viNumber, String model, int year, double price, int bodyStyle, boolean sunRoof, boolean leatherInterior) {
		super(viNumber, model, year, price);
		this.bodyStyle = bodyStyle;
		this.sunRoof = sunRoof;
		this.leatherInterior = leatherInterior;
	}

	public int getBodyStyle() {
		return this.bodyStyle;
	}

	public boolean hasSunRoof() {
		return this.sunRoof;
	}

	public boolean hasLeatherInterior() {
		return this.leatherInterior;
	}

	@Override
	public void printDetails() {
		super.printDetails();
		System.out.println("Body Style: " + this.bodyStyle + " doors");
		System.out.println("Sun Roof: " + (this.sunRoof ? "Included" : "Excluded"));
		System.out.println("Leather Interior: " + (this.leatherInterior ? "Included" : "Excluded"));
	}

	@SuppressWarnings("serial")
	@Override
	public String getAdditionalInfo() {
		return new HashMap<String, String>() {
			{
				this.put("Body Style", bodyStyle + " doors");
				this.put("Sun Roof", (sunRoof ? "Included" : "Excluded"));
				this.put("Leather Interior", (leatherInterior ? "Included" : "Excluded"));
			}
		}.toString();
	}
}
