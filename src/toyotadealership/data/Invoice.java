package toyotadealership.data;

import java.text.SimpleDateFormat;

import toyotadealership.car.BaseCar;

public class Invoice {

	private String customerName;
	private String customerAddress;
	private String customerPhoneNumber;

	private BaseCar soldCar;
	private long dateOfSale;
	private double totalPrice;

	public Invoice(String customerName, String customerAddress, String customerPhoneNumber, BaseCar soldCar, long dateOfSale, double totalPrice) {
		this.customerName = customerName;
		this.customerAddress = customerAddress;
		this.customerPhoneNumber = customerPhoneNumber;
		this.soldCar = soldCar;
		this.dateOfSale = dateOfSale;
		this.totalPrice = totalPrice;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public String getCustomerAddress() {
		return this.customerAddress;
	}

	public String getCustomerPhoneNumber() {
		return this.customerPhoneNumber;
	}

	public BaseCar getCar() {
		return this.soldCar;
	}

	public long getDateOfSale() {
		return this.dateOfSale;
	}

	public double getTotalPrice() {
		return this.totalPrice;
	}

	public void printSalesDetails(SimpleDateFormat dateFormat) {
		System.out.println("Customer Name: " + this.customerName);
		System.out.println("Customer Address: " + this.customerAddress);
		System.out.println("Customer Phone Number: " + this.customerPhoneNumber);
		System.out.println("Date of Sale: " + dateFormat.format(this.dateOfSale));
		this.soldCar.printDetails();
		System.out.printf("Total Price: $%.2f\n", this.totalPrice);
	}
}
